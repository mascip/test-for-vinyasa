For every example that I give, I restart a brand new REPL

I didn't copy the outputs, but I indicate every time there is an exception.
If nothing follows a command, it means that command was successful

# Learning about namespaces...

## (vinyasa.graft/graft '[clojure.pprint pp])

### graft works

    user=> (pp/pprint 1)
    user=> (in-ns 'test-vinyasa.core)
    test-vinyasa.core=> (pp/pprint 1)


## (vinyasa.inject/inject 'clojure.core '> '[ [clojure.pprint pprint] ])

### Inject does not work after in-ns...

    user=> (>pprint 1)
    user=> (in-ns 'test-vinyasa.core)
    test-vinyasa.core=> (>pprint 1)
        => Unable to resolve symbol: >pprint
    test-vinyasa.core=> (clojure.core/>pprint 1)

###... but Inject works after (refer-clojure)

    test-vinyasa.core=> (clojure.core/refer-clojure)
    test-vinyasa.core=> (>pprint 1)



## From the 'user namespace, I must require test-vinyasa.core, in order to use its functions

    user=> (test-vinyasa.core/foo 1)
        => ClassNotFoundException: test-vinyasa.core
    user=>(require 'test-vinyasa.core)
    user=> (test-vinyasa.core/foo 1)

## After in-ns, even the local functions are not found: I must also `require`

    test-vinyasa.core=> (foo 1)
        => Unable to resolve symbol: foo
    test-vinyasa.core=> (require 'test-vinyasa.core)
    test-vinyasa.core=> (foo 1)

## After requiring 'test-vinyasa.core from the 'user namespace, I can and access all the functions of 'test-vinyasa.core, from anywhere

    user=> (require 'test-vinyasa.core)
    user=> (in-ns 'test-vinyasa.core)
    test-vinyasa.core=> (foo 1)
    test-vinyasa.core=> (in-ns 'some-new-namespace)
    some-new-namespace=> (test-vinyasa.core/foo 1)

# Solutions that DON'T work:

From now on, 'another-namespace is a namespace required by the 'test-vinyasa.core namespace (see the file src/test-vinyasa/core.clj)

## If I declare test-vinyasa.core as the "main" namespace in the project.clj file, then this namespace (and all the ones it requires) is loaded (in project.clj: :main test-vinyasa.core), but NOT the injected functions...

    user=> (another-namespace/bar)
    user=> (in-ns 'another-namespace)
    another-namespace=> (bar)
    another-namespace=> (>pprint 1)
        => Unable to resolve symbol: >pprint

## Require 'clojure.core and 'test.vinyasa.core from user.clj: (ns user (:require [clojure.core] [test-vinyasa.core])). The injected functions cannot be accessed after doing (in-ns)

    user=> (in-ns 'another-namespace)
    another-namespace=> (bar)
    another-namespace=> (>pprint 1)
        => Unable to resolve symbol: >pprint

# Solutions that WORK:

##  SOLUTION 1: When I require 'test-vinyasa.core, the functions in 'another-namespace become available, as well as the injected functions

    user=> (require 'test-vinyasa.core)
    user=> (another-namespace/bar)
    user=> (in-ns 'another-namespace)
    another-namespace=> (bar)
    another-namespace=> (>pprint 1)

## SOLUTION 2: If I use `refresh` (from clojure.tools.namespace.repl) in the 'user namespace, then all functions become available everywhere

    user=> (refresh)
    user=> (another-namespace/bar)
    user=> (in-ns 'another-namespace)
    another-namespace=> (bar)
    another-namespace=> (>pprint 1)


## But, weirdly enough, adding this in profiles.clj doesn't work:

    :repl-options {
      :init (do
        (require 'clojure.tools.namespace.repl)
        (clojure.tools.namespace.repl/refresh))}

## And doing (refresh) in user.clj doesn't work either. In fact, the REPL doesn't even start and throws an exception
